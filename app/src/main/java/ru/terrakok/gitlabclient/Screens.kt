package ru.terrakok.gitlabclient

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 26.03.17.
 */
object Screens {
    const val AUTH_FLOW = "auth flow"
    const val AUTH_SCREEN = "auth screen"

    const val MAIN_FLOW = "main flow"
    const val MAIN_SCREEN = "main screen"
    const val PROJECTS_SCREEN = "projects screen"
    const val ABOUT_SCREEN = "about screen"
    const val APP_LIBRARIES_SCREEN = "app libraries screen"

    const val PROJECT_FLOW = "project flow"
    const val PROJECT_INFO_SCREEN = "project info screen"

    const val USER_FLOW = "user flow"
    const val USER_INFO_SCREEN = "user info screen"

    const val MR_FLOW = "mr flow"
    const val MR_INFO_SCREEN = "mr info screen"

    const val ISSUE_FLOW = "issue flow"
    const val ISSUE_INFO_SCREEN = "issue info screen"
}